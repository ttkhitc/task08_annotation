package task08_Annotation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class InformationAboutAnotation {

	@TarasAnotation(myAge = 25)
	private int myAge;

	private int b = 8;

	@TarasAnotation(name = "s")
	private String s;

	private String k = "Tratata";

	public void chechAnotation() {

		Class<InformationAboutAnotation> info = InformationAboutAnotation.class;

		Field[] fields = info.getDeclaredFields();

		System.out.println("Declared field with @TarasAnotation ");

		for (Field k : fields) {
			k.setAccessible(true);
			if (k.isAnnotationPresent(TarasAnotation.class)) {
				System.out
						.print(Modifier.toString(k.getModifiers()) + " " + k.getType().toString() + " " + k.getName());
				System.out.print(
						", have anotation @TrarasAnotation(name = \"" + k.getAnnotation(TarasAnotation.class).name()
								+ "\" , myAge = \"" + k.getAnnotation(TarasAnotation.class).myAge());
				System.out.println("\n");
			}
		}
	}

	public void myMethod(String string, int... args) {

		System.out.println(string);

		for (int s : args)

			System.out.print(" " + s);

	}

	public void myMethod(String... args) {
		StringBuilder bilder = new StringBuilder();

		for (String string : args) {

			System.out.println(string);
		}
System.out.println("\n");
	}

	public static void Invoker(Object obj) {

		try {

			Method m = obj.getClass().getDeclaredMethod("myMethod", String[].class);

			System.out.println(m.invoke(obj,
					new Object[] { new String[] { "Hello", "NoProblem", "By"} }));

			m = obj.getClass().getDeclaredMethod("myMethod", String.class, int[].class);

			m.invoke(obj, new Object[] { "Nums  ",
					new int[] { 32, 34, 76, 989, 123, 45, 576, 79, 324, 12, 45, 8, 345, 123, 436, 2134, 56, 3 } });

		} catch (NoSuchMethodException e) {
			System.out.println("Problem \n");
			
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			System.out.println("Problem \n");
			
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			System.out.println("Problem \n");
	
			e.printStackTrace();
		}
	}

	public void InformationAboutMethods(Object s) {

		Class clas = s.getClass();

		Method[] methods = clas.getDeclaredMethods();

		for (Method met : methods) {

			System.out.println("Name  " + met.getName());

			System.out.println("Return type of method " + met.getReturnType());

			if (met.getParameterCount() == 0)

				System.out.println("No parameters in this method");

			for (Class clases : met.getParameterTypes()) {

				System.out.println("Parameter is  " + clases.getCanonicalName());
				System.out.println("\n");
			}
		}
	}

	public static void main(String[] args) {

		InformationAboutAnotation information = new InformationAboutAnotation();

		information.chechAnotation();

		information.InformationAboutMethods(information);

		Invoker(information);

	}
}
